TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Validators/Source/SequenceValidator.cpp \
    Validators/Source/LengthValidator.cpp \
    Validators/Source/DigitValidator.cpp \
    Validators/Source/AlphaValidator.cpp \
    Validators/Source/AllowedSymbolsValidator.cpp \
    Validators/Source/StringValidator.cpp \
    Utils/Source/ConsoleNotifier.cpp \
    Utils/Source/StdInStringReciever.cpp

HEADERS += \
    ValidationRunner.h \
    StringValidator.h \
    SequenceValidator.h \
    LengthValidator.h \
    DigitValidator.h \
    AlphaValidator.h \
    AllowedSymbolsValidator.h \
    ValidationRunner.h \
    Utils/Headers/ValidationRunner.h \
    Validators/Headers/StringValidator.h \
    Validators/Headers/SequenceValidator.h \
    Validators/Headers/LengthValidator.h \
    Validators/Headers/DigitValidator.h \
    Validators/Headers/AlphaValidator.h \
    Validators/Headers/AllowedSymbolsValidator.h \
    Utils/Headers/ConsoleNotifier.h \
    Utils/Headers/StdInStringReciever.h

INCLUDEPATH += Validators/Headers \
                Utils/Headers
QMAKE_CXXFLAGS += -std=c++11
