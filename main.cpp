#include <iostream>

#include "ValidationRunner.h"

int main(int argc, char* argv[])
{
    ValidationRunner<>().Execute( {argv, argv + argc} );
    return 0;
}

