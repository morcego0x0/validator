# OVERVIEW #

The project is the ipelmentation of test task.

The only objective of this test task is to validate 
input string according to the some sort of predefined rules.

####What rules are used by default

* Validate by string length - by default the lebgth of the string should be between 5 and 12(both inclusive)
* At least one digit and one letter should be present in the specified string.
* There are no one adjacent same substring should be present in the string. The modified KMP algorithm is used to find such sequence.

### BUILD STEPS ###

The project was implemented using QtCreator. So to build the sources this one is requiered.

* Open Validator.pro file via QtCreator configure project if required and press `Ctrl` + `R` to run.
* Open UnitTest.pro file via QtCreator in order to build tests. Configure project if required and press `Ctrl` + `R` to run.

### Run without building ###
The project contains Build directory. In this directory you can find binaries.

#### Execute program on Linux ####
* Go to Build/Debug(Release)/Linux
* Run ./Validator [string for validation]

#### Execute program on Windows ####
* Go to Build/Debug(Release)/Windows
* Run Validator.exe [string for validation]

#### Execute tests on Linux ####
* Go to Tests/Build/Debug(Release)/Linux
* Run ./UnitTest

#### Execute tests on Windows ####
* Go to Test/Build/Debug(Release)/Windows
* Unpack Windows.7z archive whereever you want.
* Run UnitTest.exe