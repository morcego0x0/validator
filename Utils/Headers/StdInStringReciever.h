#ifndef STDINSTRINGRECIEVER_H
#define STDINSTRINGRECIEVER_H

#include <string>
#include <vector>

/**
 * @brief The StdInStringReciever class Used as a default policy
 * in the @ValidationRunner. This class allows to obtain string for the validation
 * from stdin.
 */
class StdInStringReciever
{
public:
    /**
     * @brief StdInStringReciever creates new reciever object.
     */
    StdInStringReciever();

    /**
     * @brief ObtainString parse specified command line to obtain
     * string for the validation.
     *
     * @param commandLine The command line arguments
     *
     * @return string for validation or an empty string in case of error.
     */
    std::string ObtainString(const std::vector<std::string> &commandLine) const;
private:
    std::string stdInString;
};

#endif // STDINSTRINGRECIEVER_H
