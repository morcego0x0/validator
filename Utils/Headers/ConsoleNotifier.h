#ifndef CONSOLENOTIFIER_H
#define CONSOLENOTIFIER_H

#include <string>
#include <iostream>

/**
 * @brief The ConsoleNotifier class Used as the policy class
 * for reporting about validation result. This one uses as default notifier
 * int the @ValidationRunner class.
 */
class ConsoleNotifier
{
public:
    /**
     * @brief ConsoleNotifier allows to create new notifier.
     */
    ConsoleNotifier();

    /**
     * @brief NotifyResult notifies validation result to the stdout
     *
     * @param who that validated string
     *
     * @param what the validation result.
     */
    void NotifyResult(const std::string &who, const std::string &what);
};

#endif // CONSOLENOTIFIER_H
