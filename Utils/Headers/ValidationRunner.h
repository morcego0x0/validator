#ifndef VALIDATIONRUNNER_H
#define VALIDATIONRUNNER_H

#include "StringValidator.h"

#include "StdInStringReciever.h"
#include "ConsoleNotifier.h"
#include "AlphaValidator.h"
#include "DigitValidator.h"
#include "LengthValidator.h"
#include "SequenceValidator.h"
#include "AllowedSymbolsValidator.h"

#include <memory>
#include <list>
#include <string>
#include <algorithm>
#include <vector>
#include <initializer_list>
/**
 * @brief The ValidationRunner class allows to run specified string
 * across the list of the validators.
 *
 * The class usese polices to configure the place from which the string
 * for validatin will be obtained, as well as the place in which
 * notifications should be sent.
 *
 * There is two predefined policy @StdInStringReciever which allows
 * to obtain string for validatin from the command line.
 * And @ConsoleNotifier that allows to sent notifications to the stdout.
 */
template <
        class StringRecievePolicy = StdInStringReciever,
        class NotifyPolicy = ConsoleNotifier
         >
class ValidationRunner: public StringRecievePolicy, public NotifyPolicy
{
public:
    /**
     * @brief ValidationRunner creates new runner
     * This constructor allows to use default validators set.
     */
    ValidationRunner()
        : validatorsChain({
                            std::make_shared<AlphaValidator>(),
                            std::make_shared<DigitValidator>(),
                            std::make_shared<LengthValidator>(),
                            std::make_shared<SequenceValidator>(),
                            std::make_shared<AllowedSymbolsValidator>()
                        })
    {}

    /**
     * @brief ValidationRunner one more constructor that allows to initialize
     * class using list of validators specified by user
     *
     * @param validators The validators that user has been specified
     */
    ValidationRunner(std::initializer_list<std::shared_ptr<StringValidator>> validators)
    {
        validatorsChain.insert(validatorsChain.end(), validators.begin(), validators.end());
    }

    /**
     * @brief AddValidator allows to add one more validator to the validators list.
     *
     * @param validator
     */
    void AddValidator(std::shared_ptr<StringValidator> validator)
    {
        validatorsChain.push_back(validator);
    }

    /**
     * @brief RemoveValidator allows to remove specified validator form the validators list.
     *
     * @param validator
     */
    void RemoveValidator(std::shared_ptr<StringValidator> validator)
    {
        validatorsChain.remove(validator);
    }

    /**
     * @brief RemoveAllValidators allows to clear validators list.
     */
    void RemoveAllValidators()
    {
        validatorsChain.clear();
    }

    /**
     * @brief Execute executes ValidationRunner. The method obtains string
     * for validation aacording to the recieve policy.
     *
     * According to the default policy for validated string
     * the function obtain command line string array where
     * first element is programm name and the second is the string that
     * should be validated.
     *
     * @param commandLine
     */
    void Execute(const std::vector<std::string> &commandLine)
    {
        std::string str = std::move(this->ObtainString(commandLine));
        if (!str.empty())
        {
            StartValidation(str);
        }
    }

    /**
     * @brief StartValidation allows to run validation chain.
     *
     * @param str string to validate.
     *
     * @return true in case when all validators pass fine, false otherwise.
     */
    bool StartValidation(const std::string &str)
    {

        // Run validation chain. It executes before first fail.
        bool result = (validatorsChain.end() ==
                      std::find_if_not( validatorsChain.begin(), validatorsChain.end(), [&str, &result, this]
                                        (std::shared_ptr<StringValidator> validator)
                                        {
                                            result = validator->Validate(str);
                                            if (!result)
                                            {
                                                this->NotifyResult(str, validator->GetResult());
                                            }
                                            return result;
                                        }));

        return result;
    }

     /**
      * @brief clear validators list.
      */
    ~ValidationRunner() {}

private:
    std::list<std::shared_ptr<StringValidator> > validatorsChain;
};

#endif // VALIDATIONRUNNER_H
