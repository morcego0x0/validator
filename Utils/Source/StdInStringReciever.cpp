#include "StdInStringReciever.h"

#include <iostream>

StdInStringReciever::StdInStringReciever()
{
}

std::string StdInStringReciever::ObtainString(const std::vector<std::string> &commandLine) const
{
    if (commandLine.size() != 2)
    {
        std::cout << "Wrong command line arguments list" << std::endl;
        std::cout << "Usage:" << commandLine[0] << " [string to validate]"<< std::endl;
    }

    return commandLine[1];
}
