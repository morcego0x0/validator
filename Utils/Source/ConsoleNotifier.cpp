#include "ConsoleNotifier.h"

ConsoleNotifier::ConsoleNotifier()
{

}

void ConsoleNotifier::NotifyResult(const std::string &who, const std::string &what)
{
    std::cout <<"["<< who << "] " << what << std::endl;
}
