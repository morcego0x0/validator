#ifndef ALPHAVALIDATOR_H
#define ALPHAVALIDATOR_H

#include "StringValidator.h"

#include <string>

/**
 * @brief The AlphaValidator class allows to ensure that
 * at least one letter is present in the specified string.
 */
class AlphaValidator : public StringValidator
{
public:
    /**
     * @brief Tthe AlphaValidator constructor allows to construct new validator.
     */
    AlphaValidator();

    /**
     * @brief Validate the function allows to ensure that string
     * contains at least one letter.
     *
     * @param str - the string that should be validated.
     *
     * @return true in case if validation pass fine, false otherwise.
     */
    virtual bool Validate(const std::string &str);
};

#endif // ALPHAVALIDATOR_H
