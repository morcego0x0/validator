#ifndef STRINGVALIDATOR_H
#define STRINGVALIDATOR_H

#include <string>

/**
 * @brief The StringValidator clas is the interface class
 * for the variety validators
 */
class StringValidator
{
public:
    /**
     * @brief Validate the function will be overriden in the descendant classes.
     * @param str - the string that should be validated.
     * @return true in case if validation pass fine, false otherwise.
     */
    virtual bool Validate(const std::string &str) = 0;

    /**
     * @brief GetResult returns validation result in user friendly format.
     *
     * @return The reason of the failure or an empty string of no error occurs.
     */
    std::string GetResult() const;

protected:
    /**
     * @brief SetResult allows to set validation result.
     *
     * @param result string that contains reason of the failure.
     */
    void SetResult(std::string result);

private:
    std::string validationResult;
};

#endif // STRINGVALIDATOR_H
