#ifndef DIGITVALIDATOR_H
#define DIGITVALIDATOR_H

#include "StringValidator.h"

#include <string>

/**
 * @brief The DigitValidator class allows to check if string contains
 * at least one digit.
 */
class DigitValidator : public StringValidator
{
public:
   /**
    * @brief Tthe DigitValidator constructor allows to construct new validator.
    */
    DigitValidator();

    /**
     * @brief Validate the function allows to ensure that string
     * contains at least one digit.
     *
     * @param str - the string that should be validated.
     *
     * @return true in case if validation pass fine, false otherwise.
     */
    virtual bool Validate(const std::string &str);
};

#endif // DIGITVALIDATOR_H
