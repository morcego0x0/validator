#ifndef SEQUENCEVALIDATOR_H
#define SEQUENCEVALIDATOR_H

#include "StringValidator.h"

#include <string>

/**
 * @brief The SequenceValidator class allows to ensure that
 * there are no sequence of characters followed by the same sequence.
 */
class SequenceValidator : public StringValidator
{
public:
    /**
     * @brief Tthe AllowedSymbolsValidator constructor allows to construct new validator.
     */
    SequenceValidator();

    /**
     * @brief Validate the function allows to ensure that
     * there are no sequence of characters followed by the same sequence.
     *
     * @param str - the string that should be validated.
     *
     * @return true in case if validation pass fine, false otherwise.
     */
    virtual bool Validate(const std::string &str);
};

#endif // SEQUENCEVALIDATOR_H
