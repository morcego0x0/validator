#ifndef LENGTHVALIDATOR_H
#define LENGTHVALIDATOR_H

#include "StringValidator.h"

#include <string>

/**
 * @brief The LengthValidator class allows to validate
 * specified string according to the min and max lenght.
 */
class LengthValidator : public StringValidator
{
public:
    /**
     * @brief LengthValidator allows to construct new validator.
     * The constructor sets default value for min and max string length.
     * The default value for min is 5 and for max is 12.
     */
    LengthValidator();

    /**
     * @brief SetMinLen - allows to set minimal allowed length value for the string.
     *
     * @param len minimal allowed string length.
     */
    void SetMinLen(std::size_t len);

    /**
     * @brief GetMinLen allows to obtain minimal allowed string length.
     *
     * @return minimal allowed string length.
     */
    std::size_t GetMinLen() const;

    /**
     * @brief SetMaxLen - allows to set maximum allowed length value for the string.
     *
     * @param len maximum allowed string length.
     */
    void SetMaxLen(std::size_t len);

    /**
     * @brief GetMinLen allows to obtain minimal allowed string length.
     *
     * @return maximum allowed string length.
     */
    std::size_t GetMaxLen() const;

    /**
     * @brief Validate the function that do validation of the specified string
     * according to the specified min and max values.
     *
     * @param str - the string that should be validated.
     *
     * @return true in case if validation pass fine, false otherwise.
     *
     * @throws runtime_error in case if specified minimal length value is grater
     * then maximal
     */
    virtual bool Validate(const std::string &str);

private:
    std::size_t minValue;
    std::size_t maxValue;
};

#endif // LENGTHVALIDATOR_H
