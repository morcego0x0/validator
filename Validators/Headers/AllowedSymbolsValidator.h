#ifndef ALLOWEDSYMBOLSVALIDATOR_H
#define ALLOWEDSYMBOLSVALIDATOR_H

#include "StringValidator.h"

#include <string>

/**
 * @brief The AllowedSymbolsValidator class allows to ensure that only
 * allowed symbols are in the specified string.
 */
class AllowedSymbolsValidator : public StringValidator
{
public:
    /**
     * @brief Tthe AllowedSymbolsValidator constructor allows to construct new validator.
     */
    AllowedSymbolsValidator();

    /**
     * @brief Validate the function allows to ensure that string
     * contains only allowed symbols.
     *
     * @param str - the string that should be validated.
     *
     * @return true in case if validation pass fine, false otherwise.
     */
    virtual bool Validate(const std::string &str);
};

#endif // ALLOWEDSYMBOLSVALIDATOR_H
