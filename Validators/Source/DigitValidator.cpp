#include "DigitValidator.h"

#include <algorithm>

DigitValidator::DigitValidator()
{
}

bool DigitValidator::Validate(const std::string &str)
{
    const bool result = (str.end() != std::find_if(str.begin(), str.end(), ::isdigit));

    if (!result)
        SetResult("At least one digit should be present.");

    return result;
}
