#include "LengthValidator.h"

#include <stdexcept>
#include <sstream>

LengthValidator::LengthValidator()
    : minValue(5)
    , maxValue(12)
{
}

void LengthValidator::SetMinLen(std::size_t len)
{
    minValue = len;
}

std::size_t LengthValidator::GetMinLen() const
{
    return minValue;
}

void LengthValidator::SetMaxLen(std::size_t len)
{
    maxValue = len;
}

std::size_t LengthValidator::GetMaxLen() const
{
    return maxValue;
}

bool LengthValidator::Validate(const std::string &str)
{
    const size_t strLen = str.length();

    if (minValue > maxValue)
        throw std::runtime_error("The minimal length value can't be grater then maximal.");

    const bool result = (strLen >= minValue && strLen <= maxValue);

    if (!result)
    {
        std::ostringstream msg;
        msg << "The string length out of bound.";
        msg << "It should be between " << minValue << " and " << maxValue;
        SetResult(msg.str());
    }

    return result;
}
