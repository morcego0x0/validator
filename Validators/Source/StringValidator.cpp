#include "StringValidator.h"

std::string StringValidator::GetResult() const
{
    return validationResult;
}

void StringValidator::SetResult(std::string result)
{
    validationResult = std::move(result);
}
