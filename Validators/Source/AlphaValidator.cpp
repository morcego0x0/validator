#include "AlphaValidator.h"

#include<algorithm>

AlphaValidator::AlphaValidator()
{
}

bool AlphaValidator::Validate(const std::string &str)
{
    const bool result = (str.end() != std::find_if(str.begin(), str.end(), ::isalpha));

    if (!result)
       SetResult("At least one letter should be present.");

    return result;
}

