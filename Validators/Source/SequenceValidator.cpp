#include "SequenceValidator.h"

#include <algorithm>

SequenceValidator::SequenceValidator()
{
}

bool SequenceValidator::Validate(const std::string &str)
{
    const size_t strLen = str.length();

    size_t compareLen = 1;
    bool noSequence = true;
    while((compareLen <= strLen / 2) && noSequence)
    {
        std::string::const_iterator iter = str.begin();
        for (std::size_t i = 0; i < strLen - compareLen; ++i)
        {
            std::string subStr(iter + 1, iter + compareLen + 1);
            if (std::equal(iter, iter + compareLen, subStr.begin()))
            {
                noSequence = false;
                break;
            }
            ++iter;
        }
        ++compareLen;
    }

    if (!noSequence)
    {
        SetResult("There is repeated adjacent string.");
    }

    return noSequence;
}
