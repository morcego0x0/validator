#include "AllowedSymbolsValidator.h"

#include <algorithm>

AllowedSymbolsValidator::AllowedSymbolsValidator()
{
}

bool AllowedSymbolsValidator::Validate(const std::string &str)
{
    const  bool result = (str.end() == std::find_if_not(str.begin(), str.end(), ::isalnum));

    if (!result)
        SetResult("Only digits and letters are allowed.");

    return result;

}
